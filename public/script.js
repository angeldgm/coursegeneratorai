document.addEventListener('DOMContentLoaded', function() {
    const form = document.getElementById('topic-form');
    let outline1 = [];

    form.addEventListener('submit', async function(e) {
        e.preventDefault();
        const topic = document.getElementById('topic').value;
        const response = await fetch('/generate-ten-subtopics', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ "topic": topic, "level": 1 })
        });

        const data = await response.json();
        console.log(data);
        outline1 = data.outline1;
        const topicsContainer = document.getElementById('topics-container');
        topicsContainer.innerHTML = `
            <h2>Main Topics for</h2>
            <h3>${topic}</h3>
            <div id="main-topics">
                <div class="outline-container">
                    ${data.outline1.map((item, index) => `<div class="outline-item outline1-item" data-index="${index}">${item}</div>`).join('')}
                </div>
            </div>
        `;

        // On click on an item of outline 1 (main)
        const outlineItems = document.querySelectorAll('.outline1-item');
        outlineItems.forEach(item => item.addEventListener('click', async function(e) {
            e.preventDefault();
            const subtopic = e.target.textContent.trim();
            console.log(subtopic);
            const response = await fetch('/generate-ten-subtopics', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ "topic": topic, "subtopic": subtopic, "level": 2, "outline": outline1 })
            });

            const data = await response.json();
            const outline2 = data.outline2;
            const subtopicsContainer = document.getElementById('subtopics-container');
            subtopicsContainer.innerHTML = `
                <h2>Main Subtopics for</h2>
                <h3>${data.subtopic}</h3>
                <div class="outline-container">
                    ${data.outline2.map((item, index) => `<div class="outline-item outline2-item" data-index="${index}">${item}</div>`).join('')}
                </div>
            `;

            // On click on an item of outline 2 (secondary)
            const outline2Items = document.querySelectorAll('.outline2-item');
            outline2Items.forEach(item => item.addEventListener('click', async function(e) {
                e.preventDefault();
                const contentTopic = e.target.textContent.trim();
                console.log(contentTopic);
                const response = await fetch('/generate-content', {
                    method: 'POST',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ "topic": topic, "subtopic": subtopic, "contentTopic": contentTopic, "outline": outline1, "outline2": outline2 })
                });

                const data = await response.json();
                const contentContainer = document.getElementById('content-container');
                contentContainer.innerHTML = `
                    <h2>Explanation for</h2>
                    <h3>${contentTopic}</h3>
                    <div class="outline-container">
                        <p>${data.content}</p>
                    </div>
                `;
            }));
        }));
    });

    // On click of an item of outline 2 (secondary)
    // async function loadContent(e) {
    //     const topic = document.getElementById('topic').value;
    //     const subtopic = e.target.textContent.trim();
    //     const response = await fetch(`/generate-content/${topic}/${subtopic}`);
    //     const data = await response.json();
    //     if (data.content) {
    //         const contentContainer = document.querySelector('.content-container');
    //         contentContainer.innerHTML = `<div class="content-section">${data.content}</div>`;
    //     } else {
    //         alert('Failed to load content.');
    //     }
    // }

    // const outline2Items = document.querySelectorAll('.outline2-item');
    // outline2Items.forEach(item => item.addEventListener('click', loadContent));
});
