const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 3000;

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname + "/public/"))
app.use(bodyParser.json());  // Use JSON body parser
app.use(bodyParser.urlencoded({ extended: true }));

const API_URL = process.env.API_URL;
const API_TOKEN = process.env.API_TOKEN;

const headers = {
    "Authorization": `Bearer ${API_TOKEN}`
};

async function query(payload) {
    try {
        const response = await axios.post(API_URL, payload, { headers });
        return response.data;
    } catch (error) {
        console.error('Error querying the API:', error);
        return null;
    }
}

// function extractSubtopic(text) {
//     const match = text.match(/"([^"]+)"/);
//     if (match) {
//         return match[1].trim();
//     }
//     const parts = text.split(':').pop().split('.').shift().trim();
//     return parts;
// }

app.get('/', (req, res) => {
    res.render('index', { outline1: null, outline2: null, topic: null, subtopic: null, content: null, error: null });
});

function extractSubtopic(text) {
    // Normalize the text to extract the subtopic name
    return text.replace(/^\s*[\*\d]+\.\s*/, '').trim();
}

function processGeneratedText(text) {
    if (text.includes('\n')) {
        return text.split('\n')
           .filter(line => line.trim().length > 0)
           .map(line => extractSubtopic(line));
    } else {
        return [text];
    }
}

app.post('/generate-ten-subtopics', async (req, res) => {
    const topic = req.body.topic;
    const subtopic = req.body.subtopic;
    const level = req.body.level;
    const outline = req.body.outline;

    let prompt = "";
    if (level == 1) {
        prompt = `Provide ten subtopics for a course on ${topic}, separated by semicolon: `;
    } else if (level == 2) {
        prompt = `Provide ten subtopics for ${subtopic}, separated by semicolon: `;
    }
    console.log(prompt);
    const response = await query({
        inputs: prompt,
        options: { wait_for_model: true },
        parameters: { max_new_tokens: 500, return_full_text: false }
    });

    var generation = response[0]?.generated_text;
    console.log(generation);
    generation = generation.trim();
    console.log(generation);
    generation = generation.replace(prompt, '');
    console.log(generation);
    if (level == 3) {
        generation = processGeneratedText(generation);
    } else {
        if (generation.includes(";")) {
            generation = generation.split(";").map(s => s.trim().replace(/\d+\./g, '').trim());
        } else {
            generation = [generation];
        }
    }

    console.log(generation);

    if (!generation) {
        console.log("Failed to generate subtopics. Please try again.");
        res.json({ outline1: null, outline2: null, topic: null, subtopic: null, content: null, error: "Failed to generate first subtopic. Please try again." });
        return;
    }

    if (level == 1) {
        res.json({ outline1: generation, outline2: null, topic: topic, subtopic: subtopic, content: null, error: null });
    } else if (level == 2) {
        res.json({ outline1: outline, outline2: generation, topic: topic, subtopic: subtopic, content: null, error: null });
    } else {
        res.status(500).json({ error: "Invalid level" });
    }
});

app.post('/generate-content', async (req, res) => {
    const topic = req.body.topic;
    const subtopic = req.body.subtopic;
    const contentTopic = req.body.contentTopic;
    const outline = req.body.outline;
    const outline2 = req.body.outline2;

    const contentPrompt = `Write a text without numbering about "${contentTopic.trim()}" in "${subtopic.trim()}" in a course on ${topic.trim()}: `;
    const contentResponse = await query({
        inputs: contentPrompt,
        options: { wait_for_model: true },
        parameters: { max_new_tokens: 200, return_full_text: true }
    });

    var content = contentResponse[0]?.generated_text.replace(contentPrompt, '').trim().replace(contentPrompt, '');

    if (!content) {
        res.status(500).json({ error: "Failed to generate content. Please try again." });
        return;
    }

    res.json({ "topic": topic, "subtopic": subtopic, "contentTopic": contentTopic, "outline": outline, "outline2": outline2, "content": content, error: null });
});

app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
